// version 20230126
// public domain
// djb

// automatic-alternatives 1 -lcrypto

#include <unistd.h>
#include <openssl/rand.h>
#include "randombytes.h"

__attribute__((constructor))
static void startup(void)
{
  unsigned char x;
  while (RAND_bytes(&x,1) != 1)
    sleep(1);
}

void randombytes(void *x,long long xbytes)
{
  while (xbytes > 0) {
    // RAND_bytes takes int length
    // assume int is at least 32 bits

    int todo = 1048576;
    if (xbytes < 1048576) todo = xbytes;

    while (RAND_bytes(x,todo) != 1)
      sleep(1);

    x += todo;
    xbytes -= todo;
  }
}

const char *randombytes_source(void)
{
  return "openssl";
}
